package domaine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ReponseJoueurTest {
    @Test
    void reponseJoueurestBonneRenvoieBonneValeurBoolenne() {
        Expression expression = new OperationPlus(new Nombre(3), new Nombre(2));
        ExpressionRPN expressionRPN = new ExpressionRPN(expression);
        String chainereponseJoueur = "5.0";
        Float reponseJoueur = Float.parseFloat(chainereponseJoueur);
        ReponseJoueur objetReponseJoueur = new ReponseJoueur(expressionRPN, reponseJoueur);
        Assertions.assertTrue(objetReponseJoueur.estBonne());
    }
}
