package domaine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class OperationRacineCarreeTest {
    @Test
    void ConstructeurOperationRacineCarreeAvecEvaluationOperandeNegativeRetourneOperationImpossibleException() {
        Assertions.assertThrows(OperationImpossibleException.class, ()-> {
            OperationRacineCarree operationRacineCarree = new OperationRacineCarree(new Nombre(-5));
        });
    }
}
