package domaine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class NombreTest {
    @Test
    public void evaluationRetourneValeurNombre() {
        Nombre nombre = new Nombre(42);
        float evaluationNombre = nombre.evaluation();
        Assertions.assertEquals(42, evaluationNombre);
    }

    @Test
    void gerePileAjouteUnNombreDansLaPile() {
        Stack<Expression> pile = new Stack<>();
        pile.push(new Nombre(1));
        Nombre nombre = new Nombre(42);
        nombre.gerePile(pile);
        Assertions.assertEquals(List.of(new Nombre(1),new Nombre(42)), pile);
    }
}