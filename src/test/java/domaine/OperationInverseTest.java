package domaine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class OperationInverseTest {
    @Test
    void inverseDeZeroRetourneExceptionOperationImpossible() {
        Assertions.assertThrows(OperationImpossibleException.class, () -> {
            OperationInverse operationDivise = new OperationInverse(new Nombre(0));
        });
    }
}