package domaine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GenerateurExpressionTest {
    @Test
    void quantiteExpressionsInitialeNonVide() {
        GenerateurExpression generateur = new GenerateurExpression(() -> 0.5);
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> generateur.generer(0, 10));
    }

    @Test
    void tailleListeExpressionsInitialePositive() {
        GenerateurExpression generateur = new GenerateurExpression(() -> 0.5);
        Assertions.assertThrows(IllegalArgumentException.class, () -> generateur.generer(-1, 10));
    }

    @Test
    void uneSeuleExpressionEstDirectementRenvoyee() {
        GenerateurExpression generateurExpression = new GenerateurExpression(() -> 0.5);
        Expression expressionAvecUnSeulNombre = generateurExpression.generer(1,10);
        Assertions.assertEquals(5, expressionAvecUnSeulNombre.evaluation());
    }

    @Test
    void uneSeuleExpressionEstRenvoyeeParLeGenerateur() {
        GenerateurExpression generateurExpression = new GenerateurExpression(() -> 0.5);
        Expression expressionGeneree = generateurExpression.generer(5, 10);
         Assertions.assertTrue(expressionGeneree.equals(new OperationPlus(new OperationPlus(new OperationPlus(new Nombre(5),new Nombre(5)),new OperationPlus(new Nombre(5),new Nombre(5))),new Nombre(5))));

    }
}