package domaine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OperationDiviseTest {
    @Test
    public void constructeurOperationDiviseLanceArithmeticExceptionSiDivisionParZero() {
        Assertions.assertThrows(ArithmeticException.class, () -> {
            OperationDivise operationDivise = new OperationDivise(new Nombre(42), new Nombre(0));
        } );
    }

    @Test
    void methodeEvaluationRetourneDivisionOperandes() {
        OperationDivise operationDivise = new OperationDivise(new Nombre(42), new Nombre(3));
        Assertions.assertEquals(14, operationDivise.evaluation());
    }

    @Test
    void divisionParZeroRetourneExceptionOperationImpossible() {
        Assertions.assertThrows(OperationImpossibleException.class, () -> {
            OperationDivise operationDivise = new OperationDivise(new Nombre(42), new Nombre(0));
        });
    }
}