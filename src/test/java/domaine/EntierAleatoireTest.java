package domaine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class EntierAleatoireTest {
    @ParameterizedTest
    @CsvSource({"0.99,9", "0.81,8", "0.0,0"})
    void valeurEntierAleatoireStrictementInferieureAlaBorneSuperieure(double valeurPseudoAleatoire, int valeurAttendue) {
        EntierAleatoire entierAleatoire = new EntierAleatoire(10,()-> valeurPseudoAleatoire);
        Assertions.assertEquals(valeurAttendue, entierAleatoire.getValeur());
    }
}