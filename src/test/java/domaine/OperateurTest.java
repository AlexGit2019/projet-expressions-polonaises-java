package domaine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Stack;

public class OperateurTest {
    @Test
    void OperateurIndexTroisEstPlus() {
        int indexOperateur = 3;
        Operateur operateur = Operateur.values()[3];
        Assertions.assertEquals(Operateur.PLUS, operateur);
    }
    @Test
    void gerePileAjouteUneOperationAdequateDansLaPile() {
        Stack<Expression> pile = new Stack<>();
        pile.push(new Nombre(1));
        pile.push(new Nombre(42));
        Operateur.MOINS.gerePile(pile);
        Assertions.assertEquals(1, pile.size());
        Assertions.assertEquals(-41 ,pile.peek().evaluation());
        Assertions.assertTrue(pile.peek() instanceof OperationMoins);
    }
}
