package domaine;

import infrastructure.ExpressionController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExpressionControllerTest {
    @Test
    void formatageChaineEstCorrect() {
        String reponseSimulee = " 1 , 5 ";
        ExpressionController expressionController = new ExpressionController();
        Assertions.assertEquals("1.5", expressionController.formaterChaineDeCaracteres(reponseSimulee));
    }
}
