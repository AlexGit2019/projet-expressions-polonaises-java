package domaine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CollectionsTest {
    @Test
    void collectionsReverseModifieListeOriginale() {
        List<Integer> listeEntiers = List.of(1,2,4,8);
        Collections.reverse(listeEntiers);
        Assertions.assertEquals(List.of(8,4,2,1), listeEntiers);
    }
}
