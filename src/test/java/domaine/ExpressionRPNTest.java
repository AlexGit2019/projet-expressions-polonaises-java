package domaine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExpressionRPNTest {
    @Test
    void expressionRPNChainedeCaracteresAListeElementsAttendus() {
        OperationPlus operationPlus = new OperationPlus(new OperationPlus(new Nombre(3),new Nombre(2)), new OperationPlus(new Nombre(2), new Nombre(3)) );
        ExpressionRPN expressionRPN = new ExpressionRPN(operationPlus);
        Assertions.assertEquals("3 2 + 2 3 + +", expressionRPN.affichage());
    }

    @Test
    void constructeurExpressionRPNAPartirDuneChaineconstruitExpressionRPNCoherente() {
        String chaine = "3 2 + 2 3 + +";
        ExpressionRPN expressionRPN = new ExpressionRPN(chaine);
        Expression expressionAttendue = new OperationPlus(new OperationPlus(new Nombre(3),new Nombre(2)), new OperationPlus(new Nombre(2), new Nombre(3)));
        Assertions.assertEquals(new ExpressionRPN(expressionAttendue), expressionRPN);
    }

    @Test
    void toExpressionAPartirDuneExpressionRPNRenvoieExpressionCoherente() {
        Expression expressionAttendue = new OperationPlus(new OperationPlus(new Nombre(2), new Nombre(3)),new OperationPlus(new Nombre(3), new Nombre(2)));
        ExpressionRPN expressionRPN = new ExpressionRPN(expressionAttendue);
        Expression expression = expressionRPN.toExpression();
        Assertions.assertTrue(expression.equals(expressionAttendue));
    }


}
