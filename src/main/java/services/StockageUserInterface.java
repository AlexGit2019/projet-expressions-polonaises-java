package services;

import domaine.Credential;
import domaine.User;

import java.util.Optional;

public interface StockageUserInterface {
    Optional<User> authenticateUser(Credential credential);
}
