package services;

import domaine.ReponseJoueur;
import domaine.Score;
import domaine.ScoresAutourDeScore;
import domaine.User;
import infrastructure.mysql.StockageScoreMySQL;

import java.time.Instant;
import java.util.List;

public class ScoreService {
    private StockageScoreInterface stockageScoreInterface;

    public ScoreService(services.StockageScoreInterface stockageScoreInterface) {
        this.stockageScoreInterface = stockageScoreInterface;
    }
    public List<Score> recupererDixMeilleursScores() {
        return stockageScoreInterface.recupererDixMeilleursScores();
    }
    public Score recupererScore(List<ReponseJoueur> reponsesJoueurs, User user) {
        Instant instantScore = Instant.now();
        int valeurScore = 0;
        for (ReponseJoueur reponseJoueur : reponsesJoueurs) {
            if (reponseJoueur.estBonne()) {
                valeurScore++;
            }
        }
        Score score = new Score(valeurScore,user,instantScore);
        return score;

    }

    public void insereNouveauScore(Score score) {
        this.stockageScoreInterface.insererNouveauScore(score);
    }
    public ScoresAutourDeScore recupererScoresAutourDeScore(Score score) {
        return stockageScoreInterface.recupererScoresAutourDeScore(score);
    }
}
