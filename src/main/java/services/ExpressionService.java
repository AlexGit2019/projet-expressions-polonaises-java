package services;

import domaine.*;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class ExpressionService {
    public static List<ExpressionRPN> creerDixExpressionsRPN(int nombreExpressionsDeDepart, int borneSuperieureValeurExpression) {
        //Création d'un nouvel objet de la classe GenerateurExpression
        GenerateurExpression generateurExpression = new GenerateurExpression(() -> Math.random());
        //Création d'un ouvel objet List<ExpressionRPN>, qui va stocker les expressions RPN renvoyées
        List<ExpressionRPN> expressionsRPN = new ArrayList<>();
        // Génération de dix expressions, création pour chaque expression d'une ExpressionRPN qui sera ajoutée  dans la liste
        for (int i = 0; i < 10; i++) {
            Expression expression = generateurExpression.generer(nombreExpressionsDeDepart, borneSuperieureValeurExpression);
            ExpressionRPN expressionRPN = new ExpressionRPN(expression);
            expressionsRPN.add(expressionRPN);
        }

        return expressionsRPN;
    }


}
