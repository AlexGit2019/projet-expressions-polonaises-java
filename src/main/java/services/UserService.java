package services;

import domaine.Credential;
import domaine.User;

import java.util.Optional;

public class UserService {
    private StockageUserInterface stockageUser;

    public UserService(StockageUserInterface stockageUser) {
        this.stockageUser = stockageUser;
    }

    public Optional<User> retrieveUser(Credential credential) {
        return stockageUser.authenticateUser(credential);
    }
}
