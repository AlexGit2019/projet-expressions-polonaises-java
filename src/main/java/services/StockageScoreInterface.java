package services;

import domaine.Score;
import domaine.ScoresAutourDeScore;

import java.util.List;

public interface StockageScoreInterface {
    List<Score> recupererDixMeilleursScores();

    void insererNouveauScore(Score score);

    ScoresAutourDeScore recupererScoresAutourDeScore(Score score);
}
