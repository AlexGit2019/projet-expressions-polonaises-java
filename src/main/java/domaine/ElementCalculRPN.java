package domaine;

import java.util.Stack;

public interface ElementCalculRPN {
    String getRepresentation();
    void gerePile(Stack<Expression> pile);
}
