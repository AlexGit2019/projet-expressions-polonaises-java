package domaine;

import java.util.*;
import java.util.function.Function;

public enum Operateur implements ElementCalculRPN {
    MULTIPLIE(2, operandes -> new OperationMultiplie(operandes.get(0), operandes.get(1)), "*"),
    DIVISE(2, operandes -> new OperationDivise(operandes.get(0), operandes.get(1)), "/"),
    INVERSE(1, operandes -> new OperationInverse(operandes.get(0)), "inv"),
    PLUS(2, operandes -> new OperationPlus(operandes.get(0), operandes.get(1)), "+"),
    MOINS(2, operandes -> new OperationMoins(operandes.get(0), operandes.get(1)), "-"),
    RACINE_CARREE(1, operandes -> new OperationRacineCarree(operandes.get(0)), "√");

    private final int nombreOperandes;
    private final Function<List<Expression>, Expression> createurOperation;
    private final String representation;

    Operateur(int nombreOperandes, Function<List<Expression>, Expression> createurOperation, String representation) {
        this.nombreOperandes = nombreOperandes;
        this.createurOperation = createurOperation;
        this.representation = representation;
    }

    public Expression creerOperation(List<Expression> expressions) {
        Expression operation = this.createurOperation.apply(expressions);
        return operation;
    }

    public int getNombreOperandes() {
        return nombreOperandes;
    }


    @Override
    public String getRepresentation() {
        return this.representation;
    }

    @Override
    public void gerePile(Stack<Expression> pile) {
        List<Expression> expressionsRetirees = new ArrayList<>(this.nombreOperandes);
        for (int i = 0 ; i < this.nombreOperandes; i++) {
            Expression expression = pile.pop();
            expressionsRetirees.add(expression);
        }
        Collections.reverse(expressionsRetirees);
        Expression operation = this.creerOperation(expressionsRetirees);

        pile.push(operation);
    }

    public static Optional<Operateur> getOperateur(String chaine) {
        for(Operateur operateur : Operateur.values()) {
            if (chaine.equals(operateur.representation)) {
                return Optional.of(operateur);
            }
        }
        return Optional.empty();
    }


}
