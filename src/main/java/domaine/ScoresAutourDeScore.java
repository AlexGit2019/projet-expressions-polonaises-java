package domaine;

import java.util.List;
import java.util.Objects;

public class ScoresAutourDeScore {
    private List<Score> scoresSuperieurs;
    private List<Score> scoresInferieurs;

    public ScoresAutourDeScore(List<Score> scoresSuperieurs, List<Score> scoresInferieurs) {
        this.scoresSuperieurs = Objects.requireNonNull(scoresSuperieurs);
        this.scoresInferieurs = Objects.requireNonNull(scoresInferieurs);
    }

    public List<Score> getScoresSuperieurs() {
        return scoresSuperieurs;
    }

    public List<Score> getScoresInferieurs() {
        return scoresInferieurs;
    }
}
