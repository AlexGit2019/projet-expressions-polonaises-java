package domaine;

public class OperationRacineCarree extends OperationUnaire {
    public OperationRacineCarree(Expression operande) {
        super(operande);
        if (operande.evaluation() < 0) {
            throw new OperationImpossibleException("La racine carrée s'applique à un opérande >= 0");
        }
    }

    @Override
    protected float evaluation() {
        return (float) Math.sqrt(this.operande.evaluation());
    }

    @Override
    public Operateur getOperateur() {
        return Operateur.RACINE_CARREE;
    }
}
