package domaine;

import java.util.function.Supplier;

public class EntierAleatoire {
    private final int valeur;

    public EntierAleatoire(int borneMaximumExclue, Supplier<Double> generateurHasard) {
        this.valeur = (int) (generateurHasard.get() * borneMaximumExclue);
    }

    public int getValeur() {
        return valeur;
    }
}
