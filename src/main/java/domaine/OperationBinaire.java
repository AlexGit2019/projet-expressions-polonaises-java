package domaine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public abstract class OperationBinaire extends Expression {
    protected final Expression premierOperande;
    protected final Expression deuxiemeOperande;

    public OperationBinaire(Expression premierOperande, Expression deuxiemeOperande) {
        super();
        this.premierOperande = premierOperande;
        this.deuxiemeOperande = deuxiemeOperande;
    }

    @Override
    public List<ElementCalculRPN> recupererListeElementsRPN() {
        List<ElementCalculRPN> listeCompleteElementsRPN = new ArrayList<>();
        listeCompleteElementsRPN.addAll(this.premierOperande.recupererListeElementsRPN());
        listeCompleteElementsRPN.addAll(this.deuxiemeOperande.recupererListeElementsRPN());
        listeCompleteElementsRPN.add(this.getOperateur());
        return listeCompleteElementsRPN;
    }

    public abstract Operateur getOperateur();
    @Override
    public boolean equals(Object object) {
        if (object instanceof OperationBinaire) {
            if ((this.premierOperande.equals(((OperationBinaire) object).premierOperande)) && this.deuxiemeOperande.equals(((OperationBinaire) object).deuxiemeOperande)) {
                return true;
            }
            return false;
        }

        else {
            return false;
        }
    }

 }
