package domaine;

import java.util.Objects;
import java.util.Optional;

public class User {
    private String login;
    private Integer meilleurScore;

    public User(String login) {
        try {
            this.login = Objects.requireNonNull(login);
        }
        catch (NullPointerException e) {
            throw new ClientException("Le login et le mot de passe ne peuvent pas être vides");
        }
    }

    public String getLogin() {
        return this.login;
    }

    public Optional<Integer> getMeilleurScore() {
        return Optional.ofNullable(meilleurScore);
    }

    public void setMeilleurScore(Integer meilleurScore) {
        if (meilleurScore != null && meilleurScore < 0) {
            throw new ServerException("Le meilleur score ne peut pas être négatif");
        }
        this.meilleurScore = meilleurScore;
    }
}
