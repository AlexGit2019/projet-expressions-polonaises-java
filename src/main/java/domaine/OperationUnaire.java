package domaine;

import java.util.ArrayList;
import java.util.List;

public abstract class OperationUnaire extends Expression {
    protected final Expression operande;

    public Expression getOperande() {
        return this.operande;
    }

    public OperationUnaire(Expression operande) {
        super();
        this.operande = operande;
    }

    @Override
    public List<ElementCalculRPN> recupererListeElementsRPN() {
        List<ElementCalculRPN> listeElementsRPN = this.operande.recupererListeElementsRPN();
        List<ElementCalculRPN> listeElementsRPNRetournee = new ArrayList<>();
        listeElementsRPNRetournee.addAll(listeElementsRPN);
        listeElementsRPNRetournee.add(this.getOperateur());
        return listeElementsRPNRetournee;
    }

    public abstract Operateur getOperateur();

}
