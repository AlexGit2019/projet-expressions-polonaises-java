package domaine;

import java.util.List;

public class OperationMultiplie extends OperationBinaire {
    public OperationMultiplie(Expression premierOperande, Expression deuxiemeOperande) {
        super(premierOperande, deuxiemeOperande);
    }

    @Override
    protected float evaluation() {
        return premierOperande.evaluation() * deuxiemeOperande.evaluation();
    }

    @Override
    public Operateur getOperateur() {
        return Operateur.MULTIPLIE;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof OperationDivise) {
            if ((this.premierOperande.equals(((OperationDivise) object).premierOperande) && this.deuxiemeOperande.equals(((OperationDivise) object).deuxiemeOperande)) || (this.premierOperande.equals(((OperationDivise) object).deuxiemeOperande) && this.deuxiemeOperande.equals(((OperationDivise) object).premierOperande)) ) {
                return true;
            }
            return false;
        }

        else {
            return false;
        }
    }
}
