package domaine;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

public class GenerateurExpression {
    private Supplier<Double> generateurHasard;

    public GenerateurExpression(Supplier<Double> generateurHasard) {
        this.generateurHasard = Objects.requireNonNull(generateurHasard);
    }

    public Expression generer(int nombreExpressionsDeDepart, int borneSuperieureValeurExpression) {
        List<Expression> expressions = new ArrayList<>(nombreExpressionsDeDepart);
        for (int i = 0; i < nombreExpressionsDeDepart; i++) {
            Nombre nombre = new Nombre(new EntierAleatoire(borneSuperieureValeurExpression, this.generateurHasard).getValeur());
            expressions.add(nombre);
        }

        while (expressions.size() > 1) {
            int indiceOperateur = new EntierAleatoire(Operateur.values().length, this.generateurHasard).getValeur();
            Operateur operateur = Operateur.values()[indiceOperateur];
            List<Expression> operandesChoisis = new ArrayList<>(operateur.getNombreOperandes());
            for (int i = 0; i < operateur.getNombreOperandes(); i++) {
                int indiceOperandePiochee = new EntierAleatoire(expressions.size(), this.generateurHasard).getValeur();
                Expression operandePioche = expressions.remove(indiceOperandePiochee);
                operandesChoisis.add(operandePioche);
            }
            try {
                expressions.add(operateur.creerOperation(operandesChoisis));
            }
            catch (OperationImpossibleException e) {
                expressions.addAll(operandesChoisis); //La combinaison des opérandes choisies avec l'opérateur n'est pas possible
            }
        }

        return expressions.get(0);
    }
}
