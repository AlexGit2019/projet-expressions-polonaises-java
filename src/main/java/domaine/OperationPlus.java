package domaine;

public class OperationPlus extends OperationBinaire {
    public OperationPlus(Expression premierOperande, Expression deuxiemeOperande) {
        super(premierOperande, deuxiemeOperande);
    }

    @Override
    protected float evaluation() {
        return premierOperande.evaluation() + deuxiemeOperande.evaluation();
    }

    @Override
    public Operateur getOperateur() {
        return Operateur.PLUS;
    }

}
