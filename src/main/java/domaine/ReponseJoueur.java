package domaine;

import java.util.Objects;

public class ReponseJoueur {
    private ExpressionRPN expression;
    private Float reponseJoueur;

    public ReponseJoueur(ExpressionRPN expression, Float reponseJoueur) {
        this.expression = Objects.requireNonNull(expression);
        this.reponseJoueur = reponseJoueur;
    }
    public Boolean estBonne() {
        return (reponseJoueur != null && this.expression.toExpression().evaluation() == reponseJoueur);
    }
}
