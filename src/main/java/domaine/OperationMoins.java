package domaine;

public class OperationMoins extends OperationBinaire {
    public OperationMoins(Expression premierOperande, Expression deuxiemeOperande) {
        super(premierOperande, deuxiemeOperande);
    }

    @Override
    protected float evaluation() {
        return premierOperande.evaluation() - deuxiemeOperande.evaluation();
    }

    @Override
    public Operateur getOperateur() {
        return Operateur.MOINS;
    }
}
