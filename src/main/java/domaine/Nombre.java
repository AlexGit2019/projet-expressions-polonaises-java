package domaine;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class Nombre extends Expression implements ElementCalculRPN {
    private final int valeur;

    public Nombre(int valeur) {
        this.valeur = valeur;
    }

    @Override
    protected float evaluation() {
        return this.valeur;
    }

    @Override
    public List<ElementCalculRPN> recupererListeElementsRPN() {
        return List.of(this);
    }

    @Override
    public String getRepresentation() {
        return Integer.toString(this.valeur);
    }

    @Override
    public boolean equals(Object object) {
        return object instanceof Nombre && ((Nombre) object).valeur == this.valeur;
    }

    @Override
    public void gerePile(Stack<Expression> pile) {
        pile.push(this);
    }
}
