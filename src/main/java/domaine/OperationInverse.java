package domaine;

public class OperationInverse extends OperationUnaire {
    public OperationInverse(Expression operande) {
        super(operande);
        if (operande.evaluation() == 0) {
            throw new OperationImpossibleException("Opérande invalide !");
        }
    }

    @Override
    protected float evaluation() {
        return 1f / this.operande.evaluation();
    }

    @Override
    public Operateur getOperateur() {
        return Operateur.INVERSE;
    }
}
