package domaine;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Date;
import java.util.Objects;

public class Score {
    private final int score;
    private final User user;
    private final Instant date;

    public Score(int score, User user, Instant date) {
        if (score < 0 || score > 10) {
            throw new IllegalArgumentException("Le score doit être compris entre 0 et 10");
        }
        this.score = score;
        this.user = Objects.requireNonNull(user);
        this.date = Objects.requireNonNull(date);
    }



    public int getScore() {
        return score;
    }

/*
    public String getDate() {
        return new DateTimeFormatterBuilder().appendValue(ChronoField.DAY_OF_MONTH).appendLiteral("/").appendValue(ChronoField.MONTH_OF_YEAR).appendLiteral("/").appendValue(ChronoField.YEAR).toFormatter().format(this.date);
    }
*/

    public Instant getDate() {
        return date;
    }

    public User getUser() {
        return user;
    }
}
