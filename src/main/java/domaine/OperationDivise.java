package domaine;

public class OperationDivise extends OperationBinaire {
    public OperationDivise(Expression premierOperande, Expression deuxiemeOperande) {
        super(premierOperande, deuxiemeOperande);
        if (deuxiemeOperande.evaluation() == 0) {
            throw new OperationImpossibleException("Deuxième opérande invalide !");
        }
    }

    @Override
    protected float evaluation() {
        return premierOperande.evaluation() / deuxiemeOperande.evaluation();
    }

    @Override
    public Operateur getOperateur() {
        return Operateur.DIVISE;
    }

}
