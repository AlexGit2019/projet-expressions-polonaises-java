package domaine;

public class Niveau {
    private int nombreExpressionsDeDepart;
    private int borneSuperieureValeurExpression;
    private boolean expressionsTronquees;

    public Niveau(int nombreExpressionsDeDepart, int borneSuperieureValeurExpression, boolean expressionsTronquees) {
        this.nombreExpressionsDeDepart = nombreExpressionsDeDepart;
        this.borneSuperieureValeurExpression = borneSuperieureValeurExpression;
        this.expressionsTronquees = expressionsTronquees;
    }
}
