package domaine;

import domaine.Expression;

import java.util.*;

public class ExpressionRPN {
    private final List<ElementCalculRPN> expressionRPN;

    public ExpressionRPN(Expression expression) {
        this.expressionRPN = expression.recupererListeElementsRPN();
    }
    public ExpressionRPN(String representationExpression) {
        String[] tableauChainesElementsCalculRPN = representationExpression.split(" ");
        List<ElementCalculRPN> elementsCalculRPNS = new ArrayList<>();
        for (String chaineElementCalculRPN : tableauChainesElementsCalculRPN) {
            Optional<Operateur> optionalOperateur = Operateur.getOperateur(chaineElementCalculRPN);
            ElementCalculRPN elementCalculRPN;
            if (optionalOperateur.isEmpty()) {
                elementCalculRPN = new Nombre(Integer.parseInt(chaineElementCalculRPN));
            }
            else {
                elementCalculRPN = optionalOperateur.get();
            }

            elementsCalculRPNS.add(elementCalculRPN);
        }

        this.expressionRPN = elementsCalculRPNS;
    }
    public Expression toExpression() {
        Stack<Expression> pile = new Stack<>();

        for (ElementCalculRPN elementCalculRPN : this.expressionRPN) {
            elementCalculRPN.gerePile(pile);
        }
        return pile.pop();
    }

    public String affichage() {
        StringBuilder chaineAAfficher = new StringBuilder();
        boolean estPremier = true;
        for (ElementCalculRPN element : this.expressionRPN) {
            if (estPremier) {
                estPremier = false;
            }
            else {
                chaineAAfficher.append(' ');
            }
            chaineAAfficher.append(element.getRepresentation());
        }
        return chaineAAfficher.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExpressionRPN)) return false;
        ExpressionRPN that = (ExpressionRPN) o;
        return expressionRPN.equals(that.expressionRPN);
    }

    @Override
    public int hashCode() {
        return Objects.hash(expressionRPN);
    }
}
