package domaine;

import java.util.List;

public abstract class Expression {

    protected abstract float evaluation();

    public abstract List<ElementCalculRPN> recupererListeElementsRPN();
}
