package infrastructure;

import domaine.*;
import infrastructure.mysql.StockageScoreMySQL;
import services.ExpressionService;
import services.ScoreService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class ExpressionController extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        String difficultyLevel = req.getParameter("difficulty-level");
        final int nombreExpressionsDeDepart;
        final int borneSuperieureValeurExpression;
        if (difficultyLevel.equals("easy")) {
            nombreExpressionsDeDepart = 3;
            borneSuperieureValeurExpression = 5;
        }
        else {
            nombreExpressionsDeDepart = 5;
            borneSuperieureValeurExpression = 10;
        }
        List<ExpressionRPN> expressionsRPN = ExpressionService.creerDixExpressionsRPN(nombreExpressionsDeDepart,borneSuperieureValeurExpression);
        List<String> chainesDeCaracteresExpressionsRPN = new ArrayList<>();
        for (ExpressionRPN expressionRPN : expressionsRPN) {
            chainesDeCaracteresExpressionsRPN.add(expressionRPN.affichage());
        }
        req.setAttribute("chainesDeCaracteresExpressionsRPN", chainesDeCaracteresExpressionsRPN);
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/ExpressionsPolonaises.jsp");
        dispatcher.forward(req, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        List<ReponseJoueur> reponsesJoueur = recupererReponses(req);
        User user = (User) req.getSession().getAttribute("loggedUser");
        ScoreService scoreService = new ScoreService(new StockageScoreMySQL());
        Score score = scoreService.recupererScore(reponsesJoueur, user);
        if (user.getMeilleurScore().isEmpty() || score.getScore() > user.getMeilleurScore().get()) {
            user.setMeilleurScore(score.getScore());
        }
        try {
            ScoresAutourDeScore scoresAutourDeScore = scoreService.recupererScoresAutourDeScore(score);
            req.setAttribute("scoresAutourDeScore", scoresAutourDeScore);
            scoreService.insereNouveauScore(score);
            req.setAttribute("score", score);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/finDePartie.jsp");
            requestDispatcher.forward(req,resp);
        }
        catch (ServerException e) {
            resp.sendError(500, e.getMessage());
        }

    }

    private List<ReponseJoueur> recupererReponses(HttpServletRequest req) {
        List<ReponseJoueur> objetsReponseJoueurs = new ArrayList<>();
        Map<String, String[]> parameters = req.getParameterMap();
        Set<Map.Entry<String,String[]>> setParameters = parameters.entrySet();
        for (Map.Entry<String,String[]> parametre : setParameters) {
            ExpressionRPN expressionRPN = new ExpressionRPN(parametre.getKey());
            Float reponseJoueur;
            String chaineReponseJoueur = parametre.getValue()[0];
            if (chaineReponseJoueur.isBlank()) {
                reponseJoueur = null;
            }
            else {
                chaineReponseJoueur = this.formaterChaineDeCaracteres(chaineReponseJoueur);
                try {
                    reponseJoueur = Float.parseFloat(chaineReponseJoueur);
                }
                catch (NumberFormatException e) {
                    reponseJoueur = null;
                }
            }
            ReponseJoueur objetReponseJoueur = new ReponseJoueur(expressionRPN, reponseJoueur);
            objetsReponseJoueurs.add(objetReponseJoueur);
        }

        return objetsReponseJoueurs;
    }

    /*TODO parcourir la chaîne de caractères :
        -- Si il y a un espace blanc, le supprimer
        -- Si il y a une virgule, la remplacer par un point
        Utiliser Character.isWhitespace()
     */
    public String formaterChaineDeCaracteres(String chaine) {
        chaine = chaine.trim();
        char[] tableauCaracteresChaine = chaine.toCharArray();
        StringBuilder chaineConstruite = new StringBuilder();
        for (int i = 0; i < tableauCaracteresChaine.length; i++) {
            if (Character.compare(tableauCaracteresChaine[i], ' ') == 0) {
                continue;
            }
            else if (Character.compare(tableauCaracteresChaine[i], ',') == 0) {
                chaineConstruite.append('.');
            }
            else {
                chaineConstruite.append(tableauCaracteresChaine[i]);
            }
        }
        return chaineConstruite.toString();
    }
}
