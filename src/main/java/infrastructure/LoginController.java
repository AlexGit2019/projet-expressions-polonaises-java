package infrastructure;

import domaine.ClientException;
import domaine.ServerException;
import domaine.Credential;
import domaine.User;
import infrastructure.mysql.StockageUserMySQL;
import org.apache.commons.codec.digest.DigestUtils;
import services.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

public class LoginController extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String login = req.getParameter("id");
        String password = req.getParameter("mdp");
        String passwdHashe = DigestUtils.sha256Hex(password);
        try {

            Credential credential = new Credential(login,passwdHashe);
            UserService userService = new UserService(new StockageUserMySQL());
            Optional<User> user = userService.retrieveUser(credential);

            if (!user.isEmpty()) {
                HttpSession session = req.getSession();
                session.setAttribute("loggedUser", user.get());
                resp.setStatus(200);
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/HomeController");
                dispatcher.forward(req,resp);
                //resp.sendRedirect(req.getContextPath() + "/");
            }
            else {
//                TODO Gérer la redirection vers la page de connexion en cas de Fail
                resp.getWriter().write("Authentification failed !");
            }
        }
        catch (ServerException e) {
            resp.sendError(500,e.getMessage());
        }
        catch (ClientException e) {
            resp.sendError(400,e.getMessage());
        }
        catch (Exception e) {
            resp.sendError(500,e.getMessage());
        }
    }
}
