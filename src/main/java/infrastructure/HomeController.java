package infrastructure;

import domaine.Score;
import infrastructure.mysql.StockageScoreMySQL;
import services.ScoreService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.rmi.ServerException;
import java.util.List;

//@WebServlet(urlPatterns = "/Acceuil.jsp")
public class HomeController extends HttpServlet {

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        chargerPageAccueil(req, res);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        chargerPageAccueil(req, resp);
    }

    private void chargerPageAccueil(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        try {
            ScoreService scoreService = new ScoreService(new StockageScoreMySQL());
            List<Score> dixMeilleursScores = scoreService.recupererDixMeilleursScores();
            req.setAttribute("dixMeilleursScores", dixMeilleursScores);
            RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/Acceuil.jsp");
            dispatcher.forward(req, res);
        }
        catch (ServerException e) {
            res.sendError(500);
        }
    }

}
