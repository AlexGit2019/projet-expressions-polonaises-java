package infrastructure.mysql;

import domaine.Score;
import domaine.ScoresAutourDeScore;
import domaine.ServerException;
import domaine.User;

import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class StockageScoreMySQL implements services.StockageScoreInterface {
    private ConnexionMySQL CONNECTION;
    private Connection connection;

    public StockageScoreMySQL() {
        this.CONNECTION = ConnexionMySQL.CONNECTION;
        connection = this.CONNECTION.getConnection();
    }

    @Override
    public List<Score>  recupererDixMeilleursScores() {
//        String query = "select u.id, login, password, dateHeure, score " +
//                "from user u " +
//                "join resultat r on u.id = r.idUser " +
//                "order by score desc limit 10;";
        String query = "select u.id, login, dateHeure, score " +
                "from users u " +
                "join resultat r on u.id = r.idUser " +
                "order by score desc limit 10;";
        try {
            ArrayList<Score> scoreArrayList = new ArrayList<Score>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                //Création de l'utilisateur ayant réalisé le score
                User utilisateur = new User(resultSet.getString("login"));
                //Création du score et ajout du score dans la liste
                Score score = new Score(resultSet.getInt("score"),utilisateur, resultSet.getTimestamp("dateHeure").toInstant());
                //Score score = new Score(resultSet.getInt("score"), resultSet.getString("login"),resultSet.getTimestamp("dateHeure").toInstant());
                scoreArrayList.add(score);
            }
            return scoreArrayList;
        } catch (SQLException e) {
            throw new ServerException("Cette application a besoin d'une base de données", e);
        }
    }

    @Override
    public void insererNouveauScore(Score score) {
        int id = recupererIdUtilisateur(score.getUser());
        String sql = "insert into resultat(score, dateHeure, idUser) values (?, ?, ?);";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setFloat(1, score.getScore());
            preparedStatement.setTimestamp(2, Timestamp.from(score.getDate()));
            preparedStatement.setInt(3, id);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new ServerException("Cette application a besoin d'une base de données", e);
        }
    }
    public int recupererIdUtilisateur(User user) {
        String sql = "select id from users where login = ?;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,user.getLogin());
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            // l'utilisateur étant authentifié, il existe forcément.
            return resultSet.getInt("id");
        }
        catch (SQLException e) {
            throw new ServerException("Cette application a besoin d'une base de données", e);
        }
    }

    @Override
    public ScoresAutourDeScore recupererScoresAutourDeScore(Score score) {
        List<Score> scoresSuperieursScoreJoueur = this.recupererScoresSuperieurs(score);
        List<Score> scoresInferieursScoreJoueur = this.recupererScoresInferieursOuEgaux(score);
        return new ScoresAutourDeScore(scoresSuperieursScoreJoueur,scoresInferieursScoreJoueur);
    }

    private List<Score> recupererScoresInferieursOuEgaux(Score score) {
        String sql = "select u.login, r.score, r.dateHeure " + "from users u " + "join resultat r on u.id = r.idUser " + "where score <= ? " + "order by score desc ;";
        List<Score> scores = new ArrayList<>(5);
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, score.getScore());
            statement.setFetchSize(5);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Instant dateScore = resultSet.getTimestamp("dateHeure").toInstant();
                User userScore = new User(resultSet.getString("login"));
                int valeurScore = resultSet.getInt("score");
                Score scoreAjoute = new Score(valeurScore, userScore, dateScore);
                scores.add(scoreAjoute);
            }
            return scores;
        }
        catch (SQLException e) {
            throw new ServerException("Cette application a besoin d'une base de données", e);
        }
    }

    private List<Score> recupererScoresSuperieurs(Score score) {
        String sql = "select u.login, r.score, r.dateHeure " + "from users u " + "join resultat r on u.id = r.idUser " + "where score > ? " + "order by score asc;";
        List<Score> scores = new ArrayList<>(5);
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, score.getScore());
            statement.setFetchSize(5);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Instant dateScore = resultSet.getTimestamp("dateHeure").toInstant();
                User userScore = new User(resultSet.getString("login"));
                int valeurScore = resultSet.getInt("score");
                Score scoreAjoute = new Score(valeurScore, userScore, dateScore);
                scores.add(scoreAjoute);
            }
            Collections.reverse(scores);
            return scores;
        }
        catch (SQLException e) {
            throw new ServerException("Cette application a besoin d'une base de données", e);
        }
    }

}
