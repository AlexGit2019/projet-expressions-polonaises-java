package infrastructure.mysql;

import domaine.ServerException;
import domaine.Credential;
import domaine.User;
import services.StockageUserInterface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class StockageUserMySQL implements StockageUserInterface {
    private ConnexionMySQL CONNECTION;
    private Connection connection;

    public StockageUserMySQL() {
        this.CONNECTION = ConnexionMySQL.CONNECTION;
        connection = this.CONNECTION.getConnection();
    }

    @Override
    public Optional<User> authenticateUser(Credential credential) {
        String query = "select u.login, r.score " +
                "from users u " +
                "left join resultat r on u.id = r.idUser " +
                "where login = ? and password = ?" +
                "order by score desc;";
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setFetchSize(1);
            statement.setString(1, credential.getLogin());
            statement.setString(2, credential.getPassword());
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String login = resultSet.getString("login");
                Integer score = resultSet.getInt("score");
                User user = new User(login);
                if (resultSet.wasNull()) {
                    user.setMeilleurScore(null);
                }
                else {
                    user.setMeilleurScore(score);
                }
                
                return Optional.of(user);
            }
            else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new ServerException("Erreur de connexion à la BDD");
        }
    }
}
