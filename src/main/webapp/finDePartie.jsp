<%--
  Created by IntelliJ IDEA.
  User: compt
  Date: 01/01/2022
  Time: 18:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Fin de partie !</title>
    <link rel="stylesheet" type="text/css" href="css/fin-de-partie.css">
</head>
<body>
    <h1>Votre score : <c:out value="${requestScope.score.score}"/> </h1>
    <table>
        <thead>
            <tr>
                <th>Joueur</th>
                <th>Score</th>
                <th>Date du score</th>
            </tr>
        </thead>
        <tbody>
            <tr><th colspan="3">Debut des scores superieurs</th></tr>
            <c:forEach var="scoreSup" items="${requestScope.scoresAutourDeScore.scoresSuperieurs}">
                <tr>
                    <td><c:out value="${scoreSup.user.login}"/></td>
                    <td><c:out value="${scoreSup.score}"/></td>
                    <td><c:out value="${scoreSup.date}"/></td>
                </tr>
            </c:forEach>
            <tr><th colspan="3">Score du joueur</th></tr>
            <tr class="player-score">
                <td><c:out value="${requestScope.score.user.login}"/></td>
                <td><c:out value="${requestScope.score.score}"/></td>
                <td><c:out value="${requestScope.score.date}"/></td>
            </tr>
            <tr><th colspan="3">Debut des scores inferieurs</th></tr>
            <c:forEach var="scoreInf" items="${requestScope.scoresAutourDeScore.scoresInferieurs}">
                <tr>
                    <td><c:out value="${scoreInf.user.login}"/></td>
                    <td><c:out value="${scoreInf.score}"/></td>
                    <td><c:out value="${scoreInf.date}"/></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <a href="HomeController">Revenir à l'accueil</a>
</body>
<script>
    if (${requestScope.score} > 5) {

    }
</script>
</html>
