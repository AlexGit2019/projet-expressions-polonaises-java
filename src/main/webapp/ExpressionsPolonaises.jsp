<%--
  Created by IntelliJ IDEA.
  User: compt
  Date: 26/12/2021
  Time: 12:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Calcul Séries Expressions Polonaises</title>
    <link rel="stylesheet" type="text/css" href="css/expressions-polonaises.css">
</head>
    <body>
        <form action="ExpressionController" method="post">
            <table>
                <thead>
                    <tr>
                        <th>Expression</th>
                        <th>Resultat</th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach var="chaineExpressionRPN" items="${requestScope.chainesDeCaracteresExpressionsRPN}">
                    <tr>
                        <td><label for="${chaineExpressionRPN}"> <c:out value="${chaineExpressionRPN}"/></label></td>
                        <td><input type="number" step="any" id="${chaineExpressionRPN}" name="${chaineExpressionRPN}" placeholder="Entrez votre réponse..."></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <button type="submit">Valider les résultats</button>
        </form>
    </body>
</html>
